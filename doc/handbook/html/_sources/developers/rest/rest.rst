
.. index::
  double: subsystem; REST

.. _REST-Subsystem:

REST — RESTful GNUnet Web APIs
==============================

.. todo:: Define REST

Using the REST subsystem, you can expose REST-based APIs or services.
The REST service is designed as a pluggable architecture. To create a
new REST endpoint, simply add a library in the form "plugin_rest_*". The
REST service will automatically load all REST plugins on startup.

**Configuration**

The REST service can be configured in various ways. The reference config
file can be found in ``src/rest/rest.conf``:

::

   [rest]
   REST_PORT=7776
   REST_ALLOW_HEADERS=Authorization,Accept,Content-Type
   REST_ALLOW_ORIGIN=*
   REST_ALLOW_CREDENTIALS=true

The port as well as CORS (cross-origin resource sharing) headers 
that are supposed to be advertised by the rest service are configurable.

.. _Namespace-considerations:

Namespace considerations
------------------------

The ``gnunet-rest-service`` will load all plugins that are installed. As
such it is important that the endpoint namespaces do not clash.

For example, plugin X might expose the endpoint "/xxx" while plugin Y
exposes endpoint "/xxx/yyy". This is a problem if plugin X is also
supposed to handle a call to "/xxx/yyy". Currently the REST service will
not complain or warn about such clashes, so please make sure that
endpoints are unambiguous.

.. _Endpoint-documentation:

Endpoint documentation
----------------------

This is WIP. Endpoints should be documented appropriately. Preferably
using annotations.
