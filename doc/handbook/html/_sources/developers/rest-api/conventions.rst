Conventions
===========

The GNUnet Web API is based in REST principles. Data resources are accessed via HTTP requests to an API endpoint. Where possible, the Web API uses the following HTTP methods for each action:

+----------+-----------------------------+
|**Method**|**Action**                   |
+==========+=============================+
|GET       |Retrieve object(s)           |
+----------+-----------------------------+
|POST      |Create new object(s)         |
+----------+-----------------------------+
|PUT       |Edit existing object(s)      |
+----------+-----------------------------+
|DELETE    |Delete existing object(s)    |
+----------+-----------------------------+
|OPTIONS   |Get allowed headers          |
+----------+-----------------------------+


