********
REST API
********

.. toctree::
   configuration
   conventions
   identity
   namestore
   gns
   peerinfo
   config


