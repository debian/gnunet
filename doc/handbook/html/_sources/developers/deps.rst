.. _Internal-dependencies:

Internal dependencies
=====================

.. todo:: Break out into per-process information?

This section tries to give an overview of what processes a typical
GNUnet peer running a particular application would consist of. All of
the processes listed here should be automatically started by
``gnunet-arm -s``. The list is given as a rough first guide to users for
failure diagnostics. Ideally, end-users should never have to worry about
these internal dependencies.

In terms of internal dependencies, a minimum file-sharing system
consists of the following GNUnet processes (in order of dependency):

-  gnunet-service-arm

-  gnunet-service-resolver (required by all)

-  gnunet-service-statistics (required by all)

-  gnunet-service-peerinfo

-  gnunet-service-transport (requires peerinfo)

-  gnunet-service-core (requires transport)

-  gnunet-daemon-hostlist (requires core)

-  gnunet-daemon-topology (requires hostlist, peerinfo)

-  gnunet-service-datastore

-  gnunet-service-dht (requires core)

-  gnunet-service-identity

-  gnunet-service-fs (requires identity, mesh, dht, datastore, core)

A minimum VPN system consists of the following GNUnet processes (in
order of dependency):

-  gnunet-service-arm

-  gnunet-service-resolver (required by all)

-  gnunet-service-statistics (required by all)

-  gnunet-service-peerinfo

-  gnunet-service-transport (requires peerinfo)

-  gnunet-service-core (requires transport)

-  gnunet-daemon-hostlist (requires core)

-  gnunet-service-dht (requires core)

-  gnunet-service-mesh (requires dht, core)

-  gnunet-service-dns (requires dht)

-  gnunet-service-regex (requires dht)

-  gnunet-service-vpn (requires regex, dns, mesh, dht)

A minimum GNS system consists of the following GNUnet processes (in
order of dependency):

-  gnunet-service-arm

-  gnunet-service-resolver (required by all)

-  gnunet-service-statistics (required by all)

-  gnunet-service-peerinfo

-  gnunet-service-transport (requires peerinfo)

-  gnunet-service-core (requires transport)

-  gnunet-daemon-hostlist (requires core)

-  gnunet-service-dht (requires core)

-  gnunet-service-mesh (requires dht, core)

-  gnunet-service-dns (requires dht)

-  gnunet-service-regex (requires dht)

-  gnunet-service-vpn (requires regex, dns, mesh, dht)

-  gnunet-service-identity

-  gnunet-service-namestore (requires identity)

-  gnunet-service-gns (requires vpn, dns, dht, namestore, identity)
