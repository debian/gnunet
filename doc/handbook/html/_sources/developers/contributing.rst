.. _GNUnet-Contributors-Handbook:

****************************
Contributing
****************************

Licenses of contributions
=========================

GNUnet is a `GNU <https://www.gnu.org/>`__ package. All code
contributions must thus be put under the `GNU Affero Public License
(AGPL) <https://www.gnu.org/licenses/agpl.html>`__. All documentation
should be put under FSF approved licenses (see
`fdl <https://www.gnu.org/copyleft/fdl.html>`__).

By submitting documentation, translations, and other content to GNUnet
you automatically grant the right to publish code under the GNU Public
License and documentation under either or both the GNU Public License or
the GNU Free Documentation License. When contributing to the GNUnet
project, GNU standards and the `GNU philosophy <https://www.gnu.org/philosophy/philosophy.html>`__ should be
adhered to.

Copyright Assignment
====================

We require a formal copyright assignment for GNUnet contributors to
GNUnet e.V.; nevertheless, we do allow pseudonymous contributions. By
signing the copyright agreement and submitting your code (or
documentation) to us, you agree to share the rights to your code with
GNUnet e.V.; GNUnet e.V. receives non-exclusive ownership rights, and in
particular is allowed to dual-license the code. You retain non-exclusive
rights to your contributions, so you can also share your contributions
freely with other projects.

GNUnet e.V. will publish all accepted contributions under the AGPLv3 or
any later version. The association may decide to publish contributions
under additional licenses (dual-licensing).

We do not intentionally remove your name from your contributions;
however, due to extensive editing it is not always trivial to attribute
contributors properly. If you find that you significantly contributed to
a file (or the project as a whole) and are not listed in the respective
authors file or section, please do let us know.

`Download Copyright Assignment here. <https://www.gnunet.org/static/pdf/copyright.pdf>`__

.. _Contributing-to-the-Reference-Manual:

Contributing to the Reference Manual
====================================

.. todo:: Move section to contrib.rst?

.. todo:: Update contrib section to reflect move to reStructuredText

-  When writing documentation, please use `gender-neutral
   wording <https://en.wikipedia.org/wiki/Singular_they>`__ when
   referring to people, such as singular "they", "their", "them", and so
   forth.

-  Keep line length below 74 characters, except for URLs. URLs break in
   the PDF output when they contain linebreaks.

-  Do not use tab characters (see chapter 2.1 texinfo manual)

-  Write texts in the third person perspective.

.. _Contributing-testcases:

Contributing testcases
======================

In the core of GNUnet, we restrict new testcases to a small subset of
languages, in order of preference:

1. C

2. Portable Shell Scripts

3. Python (3.7 or later)

We welcome efforts to remove our existing Python 2.7 scripts to replace
them either with portable shell scripts or, at your choice, Python 3.7
or later.

If you contribute new python based testcases, we advise you to not
repeat our past misfortunes and write the tests in a standard test
framework like for example pytest.

For writing portable shell scripts, these tools are useful:

* `Shellcheck <https://github.com/koalaman/shellcheck>`__, for static
   analysis of shell scripts.
* http://www.etalabs.net/sh_tricks.html,
* ``bash``-``dash`` (``/bin/sh`` on Debian) interoperability
   * `checkbashisms <https://salsa.debian.org/debian/devscripts/blob/master/scripts/checkbashisms.pl>`__,
   * https://wiki.ubuntu.com/DashAsBinSh, and
   * https://mywiki.wooledge.org/Bashism


