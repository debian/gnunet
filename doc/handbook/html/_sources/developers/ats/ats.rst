.. index:: 
   double: subsystem; Automatic transport selection

.. _ATS-Subsystem:

ATS — Automatic transport selection
===================================

ATS stands for \"automatic transport selection\", and the function of
ATS in GNUnet is to decide on which address (and thus transport plugin)
should be used for two peers to communicate, and what bandwidth limits
should be imposed on such an individual connection. 

To help ATS make an informed decision, higher-level services inform the 
ATS service about their requirements and the quality of the service 
rendered. The ATS service also interacts with the transport service to 
be appraised of working addresses and to communicate its resource allocation 
decisions. Finally, the ATS service's operation can be observed using a 
monitoring API.

The main logic of the ATS service only collects the available addresses,
their performance characteristics and the applications requirements, but
does not make the actual allocation decision. This last critical step is
left to an ATS plugin, as we have implemented (currently three)
different allocation strategies which differ significantly in their
performance and maturity, and it is still unclear if any particular
plugin is generally superior.
