Specifications
==============

On this page you can find links to our RFC-style technical protocol
specifications. We have dubbed our specifications "Living Standards (LSDs)":

-  **LSD0000**: Reserved
-  **LSD0001**: `The GNU Name System <https://lsd.gnunet.org/lsd0001>`__
-  **LSD0002**: `re:claimID <https://lsd.gnunet.org/lsd0002>`__
-  **LSD0003**: `Byzantine Fault Tolerant Set Reconciliation
   (work-in-progress) <https://lsd.gnunet.org/lsd0003>`__
-  **LSD0004**: `The R5N Distributed Hash Table
   (work-in-progress) <https://lsd.gnunet.org/lsd0004>`__
-  **LSD0005**: `The GNU Name System DID Method
   (work-in-progress) <https://lsd.gnunet.org/lsd0005>`__
-  **LSD0006**: `The 'taler' URI scheme for GNU Taler Wallet interactions
   (work-in-progress) <https://lsd.gnunet.org/lsd0006>`__
-  **LSD0007**: `The GNUnet communicators
   (work-in-progress) <https://lsd.gnunet.org/lsd0007>`__

